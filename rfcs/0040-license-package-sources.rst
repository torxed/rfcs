=======================
License Package Sources
=======================

- Date proposed: 2024-08-07
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0040

Summary
-------

Our package sources currently do not have explicit licenses.
This RFC proposes to license all Arch Linux package sources under the terms of the Zero-Clause BSD license.

Motivation
----------

NOTE: This RFC was not written by lawyers so any statements about legal things are merely the result
of research.

Arch Linux package sources (``PKGBUILD`` and strictly related files such as e.g.
``.install``, ``.desktop`` or systemd files) currently lack
a license.
This is potentially problematic as it leaves the licensing of package sources open to
interpretation.
Some users have expressed concerns about this (`ML1`_, `Parabola Licensing Issue`_).

Some people consider package sources to be non-copyrightable (`NoC1`_, `NoC2`_, `NoC3`_, `Parabola Discussion`_).
However, most other distributions have assigned (or want to assign) a license.
See `Approaches taken by other Linux distributions`_.

Package sources may or may not be copyrightable.
Whether they are may even vary from one jurisdiction to another.
For instance, `Tuchman Law argues`_ that Hello World programs are not copyrightable since they leave no room for creativity.
The same could be said about trivial ``PKGBUILDs``.
In contrast, a complex 500-line ``PKGBUILD`` is more likely to count as a creative work.
Most of our package sources will be somewhere in the middle of the those two extremes.

Thus, if package sources are copyrightable at least in some cases, we should assign a license to all
of them to be on the safe side.
If they are not copyrightable, the license would not matter much.
As such, we only stand to gain by assigning a license.

There is no clarity as to what conditions apply to a software without an explicit license.

In some jurisdictions, the default license assigned to the software is quite restrictive.

See `GitHub's interpretation`_ and this `Open Source Stack Exchange discussion`_,
and this article by `Choose a License`_.
It can be understood that not having a license is akin to undefined behavior.

By resolving this legal uncertainty for ourselves, we also implicitly aid downstream projects,
benefiting other distributions and projects, as well as companies who consume or want to modify our
packaging sources.

This RFC proposes the Zero-Clause BSD license for all package sources as it seems to be the best way
to convey how PKGBUILDs are supposed to be used.
It is a widely accepted and easily understood license and quite liberal.
While it's uncommon for a distribution to use a 0-clause license,
we believe it would provide the least amount of friction as users that use bits and pieces of our
package sources don't need to be worried about violating a license.

.. _NoC1: https://lists.archlinux.org/pipermail/aur-general/2011-February/013651.html
.. _NoC2: https://lists.archlinux.org/pipermail/aur-general/2011-February/013652.html
.. _NoC3: https://bbs.archlinux.org/viewtopic.php?pid=1883414#p1883414
.. _ML1: https://lists.archlinux.org/pipermail/aur-general/2011-February/013508.html
.. _GitHub's interpretation: https://docs.github.com/en/repositories/managing-your-repositorys-settings-and-features/customizing-your-repository/licensing-a-repository#choosing-the-right-license
.. _Open Source Stack Exchange discussion: https://opensource.stackexchange.com/questions/1371/how-can-i-place-software-in-the-public-domain
.. _Choose a License: https://choosealicense.com/no-permission/
.. _Tuchman Law argues: https://www.tuchmanlaw.com/blog/is-hello-world-copyrighted

Prior Discussions
-----------------

.. `[aur-general] AUR & Copyright from 2011`_ (whole thread)
.. `BBS License of PKGBUILD files in AUR thread from 2020`_

.. _[aur-general] AUR & Copyright from 2011: https://lists.archlinux.org/pipermail/aur-general/2011-February/013484.html
.. _BBS License of PKGBUILD files in AUR thread from 2020: https://bbs.archlinux.org/viewtopic.php?pid=1883414

Approaches taken by other Linux distributions
---------------------------------------------

Major linux distributions all have a license for their package specifications or are working towards
providing one.
MIT or 2-Clause BSD licenses are most common; some re-use upstream licenses for package
specifications.

`Void Linux uses the 2-Clause BSD License for their void-packages repository`_.

`FreeBSD uses the 2-Clause BSD License for their ports repository`_.

`Gentoo uses the GPL v2 for their package repository`_.

`Nixpkgs uses the MIT license for all packages`_.

`OpenSUSE uses a standard header`_ declaring the source to use the same license as the package
itself, unless the package is proprietary, in which case the source uses MIT.

`Debian uses a standardized machine-readable file`_ which assigns licenses to every file in the
package source.
`See here for an example`_.
It seems that many packages re-use the upstream license for the build instructions, though that is
not always the case.

Fedora package specifications are under the MIT license by default, as stated by the
`Fedora Project Contributor Agreement`_.
Contributors are free to choose other licenses.

`Alpine doesn't currently provide licenses for their package specifications`_, but is working
towards providing one.

`Parabola GNU`_ (an Arch Linux derivative) is looking for a way to solve this upstream.

.. _Debian uses a standardized machine-readable file: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
.. _OpenSUSE uses a standard header: https://en.opensuse.org/openSUSE:Specfile_guidelines#Specfile_Licensing
.. _See here for an example: https://metadata.ftp-master.debian.org/changelogs//main/a/apparmor/apparmor_3.0.8-3_copyright
.. _Fedora Project Contributor Agreement: https://docs.fedoraproject.org/en-US/legal/fpca/
.. _Nixpkgs uses the MIT license for all packages: https://github.com/NixOS/nixpkgs/blob/nixos-24.05/COPYING
.. _Alpine doesn't currently provide licenses for their package specifications: https://gitlab.alpinelinux.org/alpine/aports/-/issues/9074
.. _Void Linux uses the 2-Clause BSD License for their void-packages repository: https://github.com/void-linux/void-packages/blob/master/COPYING
.. _FreeBSD uses the 2-Clause BSD License for their ports repository: https://cgit.freebsd.org/ports/tree/COPYRIGHT
.. _Gentoo uses the GPL v2 for their package repository: https://gitweb.gentoo.org/repo/gentoo.git/tree/header.txt
.. _Parabola GNU: https://labs.parabola.nu/issues/2862

Specification
-------------

Official repositories
~~~~~~~~~~~~~~~~~~~~~
#. Create a script to scan all packages and gather all authors according to their git contributors
   and ``# Maintainer`` and ``# Contributor`` tags.
#. Create a GitLab project (called `archlinux/relicensing-discussions`) with service desk
   functionality to allow people to report their stance via email.
#. Send an email to every author containing a list of all their package contributions and inform
   them about the new license.
   Ask for them to either do nothing and thereby consent to license all their contributions under
   0BSD or to object within **four** months.
   The email should have reply-to set to the aforementioned GitLab project's service desk.
#. After four months, drop a ``LICENSE`` file into each package repository in which all the authors
   have consented to the license.
#. Periodically check the GitLab project and address/discuss any concerns or objections.
   We'll address objections on a case-by-case basis.
   In such a case, we'll have to consider whether the contribution needs to be removed and/or
   rewritten.

See also `Eric S. Raymond's licensing guide`_.
For reference how other open source projects have handled their relicensing, also see:
`MPV relicensing`_, `MPV relicensing result`_, `Dolphin relicensing`_, `Mozilla relicensing`_.

A discussion from the Parabola distribution (See `Parabola Discussion`) indicates that there are
good reasons to ask for objections rather than asking for consent:

    It would be very difficult for anyone to claim: "I did not know
    or did not intend, that these would be shared freely, for use
    and modification by anyone in the world". It is generally
    assumed by Arch users, that every contributor to every recipe in
    Arch and AUR knows fully, that the work will be published freely
    for the public use, and that anyone with a copy, is most likely
    to have taken it, specifically with intent to modify it, and
    that no special permission is required to do so.

The license does not contain individual attribution for each contributor, which would be
error-prone to write manually and hard to generate automatically.
Instead, the license uses the generic phrase "Arch Linux Contributors".
This is in line with typical approaches in other projects.
If individual attribution is needed, it can be obtained using version control history.

The email text shall be as follows:

    Title: Licensing your Arch Linux contribution as 0BSD

    Hello Arch Linux Contributor,

    You have contributed to the following packages in the past:
    <list of packages>

    We're contacting you to inform you about our plans to license our package
    sources as 0BSD. This follows the text and discussion outlined in RFC 40 [1].

    The short version is: Arch Linux package sources didn't have an explicit
    license before. In order to steer away from this uncertainty, we'd like to
    license all package sources such as PKGBUILDs and auxiliary files as 0BSD.

    IF YOU CONSENT TO THIS, you don't have to do anything.

    IF YOU OBJECT TO THIS, please reply to this email WITHIN FOUR
    MONTHS. It will automatically create an issue in our tracking system where
    we can discuss further steps.

    [1] https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/40

We'll enforce the existence of ``LICENSE`` files using a standard software solution such as
`Reuse <https://reuse.software/>`_ or by integrating it into our Arch-specific tooling.
This RFC doesn't specify a specific solution to enforce those.

.. _MPV relicensing: https://github.com/mpv-player/mpv/issues/2033
.. _MPV relicensing result: https://github.com/mpv-player/mpv/issues/2033#issuecomment-335511905
.. _Dolphin relicensing: https://dolphin-emu.org/blog/2015/05/25/relicensing-dolphin/
.. _Mozilla relicensing: https://blogs.fsfe.org/ciaran/?p=58
.. _Eric S. Raymond's licensing guide: http://www.catb.org/~esr/Licensing-HOWTO.html#id2852348

AUR
~~~
The AUR currently specifies that all packages in it could be elevated to official repositories.
This implies that their licenses need to allow for this eventuality.
As such, we should explicitly require their package sources to share the same license as the
official packages before they become eligible for promotion.

This RFC does not specify that all AUR packages will be required to have the expected license.
If they do not have them, however, they are not eligible for promotion.

#. The AUR has a `.SRCINFO check`_ in a pre-receive hook.
   In the same place, we need to add a check for a ``LICENSE`` file with the appropriate license
   text.
   In case no appropriate ``LICENSE`` file with the expected text is found, we'll warn the users in
   git's pre-receive hook that packages without a 0BSD license are not eligible for promotion to
   official repositories.
#. Update AUR submission guidelines in the Arch Wiki to include the creation of a ``LICENSE`` file.

.. _.SRCINFO check: https://gitlab.archlinux.org/archlinux/aurweb/-/blob/5dd65846d1669d67cc3a659147b3171354e5fb84/aurweb/git/update.py#L364

License Text
~~~~~~~~~~~~
    Binary files, as well as any files describing changes ("patches") to the software that is being built are excluded from this license.
    They are provided under the license terms of the software they describe
    changes for.

    Any files containing a license notice are excluded from this license. They are provided under the license terms defined in their respective notices.

    Copyright 2024 Arch Linux Contributors

    Permission to use, copy, modify, and/or distribute this software for
    any purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED “AS IS” AND THE AUTHOR DISCLAIMS ALL
    WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
    OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
    FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY
    DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
    AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
    OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Drawbacks
---------

We're getting consent via lack of objection.
This may or may not be less legally bulletproof compared to asking every past
contributor for explicit consent.
An earlier version of this RFC proposed asking every contributor for consent directly.
However, this was deemed infeasible.

We may have to rewrite some PKGBUILDs in case of objections.

This is some work and may annoy people.

Unresolved Questions
--------------------

We'll never know whether we have the complete list of contributors. This has two reasons:

#. We don't have the complete history of all of our package sources. Many packages have existed in pre-SVN times.
#. Some AUR packages might have had their contributor tags removed.

Alternatives Considered
-----------------------

#. Keeping the status quo.
   This would continue with us being in a licensing gray zone.
#. Alternative licenses.
   We did consider suggesting other licenses but ultimately settled on 0BSD for its simplicity and
   lack of attribution requirements.
   MIT or MIT-0 might be good alternatives.
   With MIT, the need to keep the license file might be cumbersome for some use cases.
   MIT-0 is nearly equivalent to 0BSD, but newer and less widely known.
   Parabola makes a case for CC0: `Parabola Discussion`_.

.. _Parabola Discussion: https://lists.parabola.nu/pipermail/dev/2022-April/008232.html
.. _Parabola Licensing Issue: https://labs.parabola.nu/issues/2862
