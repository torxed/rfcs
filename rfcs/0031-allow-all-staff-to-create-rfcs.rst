==============================
Allow all staff to create RFCs
==============================

- Date proposed: 2024-03-05
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/31

Summary
-------

Grant all Arch Linux staff members, not limited to those in packaging roles,
the privilege to initiate RFCs directly, aligning with the broad range of
topics these documents encompass.

Motivation
----------

The RFC process, as outlined in "Using RFCs," aims to enhance transparency and
leverage the full spectrum of community expertise, covering a broad range of
topics beyond packaging. The current requirement for RFC initiation, namely
support from a Developer or Package Maintainer, unintentionally restricts
contributions from non-packaging staff.

Considering RFCs address various aspects of our distribution beyond packaging,
the eligibility criteria for who can support and propose an RFC within our
staff should reflect this diversity.

The principle behind the initial RFC requirements remains valid: securing a
staff member's endorsement for an RFC is essential. This endorsement serves not
only to validate the idea but also to ensure it is guided through the
distribution's structured processes.

Specification
-------------

We propose the following amendment to the RFC process documentation to reflect
this inclusive approach: The term "Developer or Package Maintainer" will be
updated to "Arch Linux Staff" across relevant sections of the `README file`_ in
the `RFCs repository`_.

.. _`README file`: https://gitlab.archlinux.org/archlinux/rfcs/-/blob/master/README.rst
.. _`RFCs repository`: https://gitlab.archlinux.org/archlinux/rfcs

Drawbacks
---------

None.

Unresolved Questions
--------------------

None.

Alternatives Considered
-----------------------

- **Status Quo:** Continue restricting RFC support exclusively to packaging
  staff. This approach would necessitate non-packaging-related RFCs to seek
  endorsement from packaging staff, even when the proposals originate from
  within our distro staff.
