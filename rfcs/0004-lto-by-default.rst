---
date: 2021-03-26
weight: 4
---

==============
LTO by Default
==============

- Date proposed: 2021-02-04
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0004

Summary
-------

Enable link time optimization (LTO) of packages by default by adding the
`-flto` flag. This provides smaller, faster executables/DSOs, and improves
GCC diagnostics.

Motivation
----------

Arch packages are super slow, and they need to be super fast! One way to
improve optimisation is through LTO.

LTO gives GCC the capability of dumping its internal representation (GIMPLE)
to disk, so that all the different compilation units that make up a single
executable can be optimized as a single module. This expands the scope of
inter-procedural optimizations to encompass the whole program (or, rather,
everything that is visible at link time). Clang does similar LTO, but dumps
its internal representation as LLVM byte-code.

When SUSE implemented LTO, they found the overall size of installed binaries
decreased by 5%. The benefits are particularly beneficial in large C++
programs.

Some useful reading:
 * http://www.ucw.cz/~hubicka/slides/opensuse2018-e.pdf
 * https://hubicka.blogspot.com/2019/05/gcc-9-link-time-and-inter-procedural.html

Alternatives Considered
-----------------------

Remaining with super slow and unoptimised binaries.

Specification
-------------

Your friendly pacman/makepkg developers have provided a new `lto` option [1]
in makepkg.conf (to be released). This will be enabled by default and add
`-flto` to CFLAGS and CXXFLAGS. PKGBUILDs that need to disable LTO for some
reason, can add
::

 options=('!lto')

Additionally, `makepkg` will have additions to strip the LTO bytecodes from
any installed .o/.a files. [2]

Red Hat found some issues using -flto with old configure scripts [3]. These
packages can be detected by running configure with and without LTO enabled
and comparing the generated `config.h` files. We can provide a simple sed
script as a makepkg-template in devtools to fix these packages, and do a
scan of the repos to automatically detect packages that need the fix added
to their PKGBUILD.

[1] https://lists.archlinux.org/pipermail/pacman-dev/2021-March/024908.html
[2] https://lists.archlinux.org/pipermail/pacman-dev/2021-March/024911.html
[3] https://bugzilla.redhat.com/show_bug.cgi?id=1789149


Drawbacks
---------

There are still potential bugs in LTO that cause binaries to blow up in size.
These are mostly already fixed by other distributions.

Some packages will fail to build with LTO. This results in one line being
added to their PKGBUILD.

There is some increased overhead when linking.

LTO can make debugging more difficult, with inlining of function occurring
across build units. In addition, the use of LTO can cause the memory usage of
GBD to markedly increase [4]. This can be worked around by debugging in a
non-LTO build if needed (and assuming that LTO is not the cause of the issue.

[4] https://sourceware.org/bugzilla/show_bug.cgi?id=23710

Unresolved Questions
--------------------

None.

