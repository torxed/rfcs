---
date: 2021-03-02
weight: 1
---

==========
Using RFCs
==========

- Date proposed: 2021-02-03
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/1

Summary
-------

A Request for Comment (RFC) is a way for Arch Linux contributors to propose,
design, and discuss new features and changes in project direction in a focused
environment.

Motivation
----------

Finding consensus on overarching or more involved topics in Arch Linux has
historically been driven by discussions on mailing lists, in the distribution's
various IRC channels, the wiki and the forums.
While under certain circumstances this process is still valid and does work, it
has a tendency to lead to unfocused and inconclusive discussions while lacking
a transparent process.
In addition, the results of decision-making are often oblique to newcomers, as
they may only exist in the logs of chats or mailing lists, that are not
accessible to everyone.
When considering the different entities (project leader, developers, trusted
users, support staff) that Arch Linux consists of, it becomes apparent that
distribution-wide topics do not have a platform, as the specific entities have
specialized (access-restricted) communication channels and concern themselves
(sometimes exclusively) with a specialized topic.

With the wiki article `Governance And Decision Making
<https://wiki.archlinux.org/index.php/DeveloperWiki:Governance_And_Decision_Making>`_,
an attempt has been made to outline the existing decision making processes and
put them into context with one another.
The RFC process is meant to guide and transform the distribution as a whole,
in contrast to the various specialized entities (project leader, developers,
trusted users, support staff) and their respective communication channels.

To increase transparency, source and value the knowledge and inspiration of the
entire community, an RFC based process can be used to achieve a more focused
workflow for discussing overarching topics and reaching a conclusion for the
entire distribution, while archiving discussion and consensus in a standardized
form.

Specification
-------------

The repository https://gitlab.archlinux.org/archlinux/rfcs is used to explain
how to create, discuss, finalize and to host the accepted and rejected RFCs.
An RFC is filed by creating a merge request toward the repository.

Anyone is able to create an RFC. However, contributors outside of the Arch
Linux Developer or Trusted User teams are only allowed to create an RFC, if at
least one Developer or Trusted User supports the effort and helps develop it.

New RFC proposals are announced on the `arch-dev-public
<https://lists.archlinux.org/listinfo/arch-dev-public>`_ mailing list and are
open for discussion and contribution in the merge request until finalized.

Once finalized, the RFC is entering its final comment period, which is again
announced on the `arch-dev-public
<https://lists.archlinux.org/listinfo/arch-dev-public>`_ mailing list and lasts
for 14 days. The final comment period is used to gather the motion of all
participants (again as part of the merge request) to either lead to a passed
(by merging the merge request) or rejected (by rejecting the merge request)
RFC. Participants can either use GitLab's approval feature to approve of the
RFC, or leave a comment for a more nuanced motion.

The described process allows developers, trusted users, support staff and users
alike to write an RFC and propose a change in direction for the distribution.
Ideas for RFCs may be proposed and discussed by creating a new `issue
<https://gitlab.archlinux.org/archlinux/rfcs/-/issues/new>`_ using the "idea"
template. Existing ideas for RFCs can be found by browsing the `issues
<https://gitlab.archlinux.org/archlinux/rfcs/-/issues>`_.

Drawbacks
---------

The RFC workflow breaks with the old way of reaching consensus and might be
perceived as bureaucratic.

Unresolved Questions
--------------------

None.

Alternatives Considered
-----------------------

- Mailing lists: The existing mailing list infrastructure has been used to
  reach consensus on various topics over the years. However, finding the
  reached consensus in a comprehensible format is not easy and depending on
  topic can be hidden on an access restricted mailing list.
- IRC channels: The existing staff related IRC channels on freenode.net are
  frequently used to reach consensus on minor topics, which then usually find
  their way into e-mails or other announcements. However, not all staff is
  present on IRC (at all times of the day), so it is easy to miss discussions
  or any reached consensus in this medium, if it is not propagated publicly
  elsewhere.
