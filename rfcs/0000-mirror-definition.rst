---
title: "Mirror Definition"
draft: true
---

=================
Mirror Definition
=================

- Date proposed: 2024-01-09
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0000
  **update this number after RFC merge request has been filed**

Summary
-------

Before this proposal, there were a lack of requirements and definition of what a mirror is within the Arch Linux community. This RFC therefore outlines the requirements, definitions and guidelines surrounding both becoming but also maintaining and decommissioning a mirror.

Motivation
----------

Mirrors are a well known concept on a abstract level as it's one of the foundations of most Linux distributions. However, the Arch Linux specific requirements put on mirrors are unknown or `scarecely doccumented`_. The process as of writing this is also tedious and manual for mirror administrations which is not ideal for anyone participating in the mirror workload.

This RFC aims to rectify this by first defining the different types of mirrors and then introducing requirements for those types of mirrors. This RFC also outlines a new method of becoming a listed mirror and managing the mirror list via `GitLab`_ rather than `archweb`_ and `flyspray`_.

These changes would also allow for creating central tooling and pipelines optimizing the workflow around the mirror workload by extracting it to it's own project.

Some points following in this discussion:

- Defining mirror types and roles
- Defining a new versioned mirror spec
- Defining a new proposed way of managing mirrors
- Defining improved mirror minimum requirements
- Defining a "status endpoint" for mirror operators
- Defining a validation of ownership of the mirror domain
- Defining Arch Linux commitment towards community in terms of mirror packaging
- Porposal to restructure "Tier" model
- Proposal to introduce more protocols between mirrors
- Proposal of tooling to deal with above changes

What will happen with existing mirrors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Existing Arch Linux mirror metadata in the archweb will be transformed and migrated to the Gitlab repository. Mirror owners will be able to open issues and merge requests to interact with their mirror. Mirrors will be allowed a certain grace period until they might be considered for the decommission process. These criteria changes depending on the tier levels the mirror might be. 

Depending on your mirror tier and mirror configuration, your mirror might be lowered or upgraded on the new tiering scheme. During the initial migration period the mirrors shall be sorted depending on the metadata we have at hand. The mirror owners will be notified of this change if a means of valid contact was provided. 

The transition period will be long and any improvements may still be made to the whole process. 

Timeline
--------

| 3 months notice period of change | 3 months migration/grace period | 2 months clean-up phase |

Suggested start date for these changes are 1:st of March.
This would mean that the clean up phase would start at somewhere around November ideally.

Specification
-------------

Mirror Types introduction
^^^^^^^^^^^^^^^^^^^^^^^^^

Mirror types are vaguely named according to a tier-model. We `mirror administrators`_ propose the current mirror model stays the same and be represented as an integer for optimization purposes, but rename them wherever applicable to what they represent, as well as introduce a new "Tier 3" mirror type:

- Tier0: SourceTier
- Tier1: DistributionTier
- Tier2: ContentDeliveryTier
- Tier3: EdgeTier

Tier0 This is the mirror Arch Linux Packager's update packages against, and is hosted by the Arch Linux Devops team.

Tier1 This mirror type is intended to be a near real time copy of the Tier0 mirror. This puts higher demand on this mirror type as it needs to update as soon as possible with little to no delay, and uptime is paramount for the success of other mirrors following this tier.

Tier2 would be used for the mirrors that don't have a static geographical point, but rather optimizes based on the clients geographical position.

Tier3 would be a best-effort mirror closer to individual users, and will have far less requirements and follow-ups from the `mirror administrators`_.

Mirror Specification
^^^^^^^^^^^^^^^^^^^^

A `mirror specification`_ has been created and is hereby proposed as the new going forward standard for new mirror submissions. The specification is a living entity, but is versioned and should aim to be backwards compatible but no restriction on such is put in place.

The manifest aims to define what a mirror is, and must be in a machine readable format as well as being easy for humans to read. TOML is proposed as an alternative to JSON as it supports comments as well as fulfill both requirements of being human and machine readable.

Specific mirror specification versions will become deprecated followed by a discontinue as new versions are created.

Managing Mirrors
^^^^^^^^^^^^^^^^

Before this proposal, mirrors have been managed via `flyspray`_ and `archweb`_. This have been great in the past, but is somewhat slow and tedious for `mirror administrators`_ to manage. As such, it takes more work and time than it could.

The proposal is therefore that mirrors are managed via `GitLab`_ and more specifically the `mirror project`_. The latest proposed workflow is located at the `mirror project`_.

This automates the process to such a degree that `mirror administrators`_ only have to sign off on new mirrors via new mirror releases, and the mirror administrator have to do no more or less work than already required.

Another benefit is that Arch Linux can move away from publicly revieling the mirror operators e-mail address. As GitLab contact information can be used instead. Thus complying with regulatory demands.

Mirror decommission
^^^^^^^^^^^^^^^^^^^
As unfortunate as it may be, a mirror can be up for decommissioning. This action is taken to preserve and maintain the quality of mirroring for the distribution. Some issues may be handled differently if an edge case arises.


Mirrors may be decommissioned due to several reasons:
- The mirror is unreachable or unable to fulfill its service as a mirror.
- Voluntary withdrawal by the mirror owner.
- Malicious behaviour, such as attempting to serve malicious files, or domain hijacking.
- Failure to follow specifications for a prolonged amount of time, even after given grace periods.

Decommissioning is the last step after deactivation, as there would be grace periods and ongoing communication between the involved parties.

The workflow for decommissioning a mirror is quite similar:

1. Fork the `mirror project`_.
2. Delete the  `mirror definition`_  that you want to decommission or just disable the mirror due to maintenance (flip a bool).
3. Submit a new pull request *(PR)*
4. `mirror administrators`_ sign off the *PR* and merge
5. An automatic runner creates a source of truth in the root of the `mirror project`_ in a JSON format. (The runner also checks if the person doing the MR is either a `mirror administrators`_  or mirror owner. ?)
6. The source of truth is parsed by other Arch Linux projects, such as `archweb`_ and `reflector`_

Highly Recommended Mirror Specifications and Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As outlines in the `Arch Summit 2023 - Mirror Notes <https://md.archlinux.org/5I61f0yFSmysBkV8QQFU-g>`_, new requirements/recommendations should be put into place. This is to make it more clear to a mirror operators what the expectations are, especially when *"upgrading"* a mirror.

The requirements will not be enforced immediately in existing mirrors but is heavily encouraged to do as soon as possible. The requirements also acts as a form of `SLA`_ between Arch Linux and the mirror operator(s). At some point in time, the transition period will have to begin in which a proposed grace period is enacted:

- Tier 1 and 2 mirrors will have 6 months to upgrade or change their servers, which can be extended if the procedure might take a longer time.
- Tier 3 mirrors will have 12 months to transition to the new specification. 

In case of missing the grace period, additional warnings will be sent out which are then followed by removal from the official mirror list. Tier1 and Tier2 mirrors that don't follow the spec within the grace period will be downgraded to one level below after two attempts of contact without response over a period of two months. The new requirements will first be enacted only on **new** mirrors.

Recommendation and Requirements for different mirror types
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- **Tier 0** *(SourceTier)* - This is the Arch Linux source of truth for packages. It's workflow is out of this RFC's scope and should be documented elsewhere.
- **Tier 1** *(DistributionTier)* - This mirror should aim to only be a distribution to the region *(or otherwise)* and not face end users directly if possible. With no restriction imposed to face users however - especially in mirror-scarce regions.

  Tier 1 Requirements	

  - Minimum two active e-mail contacts on minimum one GitLab account
  - Active monitoring of tagged GitLab issues (initial response within 1-2 days)
  - Uptime above 99.5% per year (measured via archweb? %completion number)
  - 1Gbit/s throughput
  - 1:1 copy of T0
  - Unlimited Bandwidth usage
  - Minimum 1TB of space
  - HTTPS TLS1.2+
  - Signed domain+lastupdate
  - Static IP on rsync
  - Listdir: Necessary for basic functionality.
  - unlimited parallel downloads
  - No CDN/Multi-Peer
  - Maintenance can last no longer than one week *(after which it gets disabled, not removed)*
  - Sync once an hour on a random minute

  Tier 1 Recommendations

  - enforce rsync server downstream
  - Allow Corporate T1 assuming:
    - Required SLA *(response time)*
    - Stricter error checks
  - Signed domain+lastupdate
  - No fail2ban/rate-limiting
  - “Status JSON manifest” discovered via DNS (TXT?)
    - Paste:able via Pastebins, or on a secondary webserver
    - Also paste:able in a GitLab issue

- **Tier 2** *(ContentDeliveryTier)* - Is according to this RFC a `CDN`_ category where servers are not bound to one static IP tied to a specific geographical position. This proposed new type will help Arch Linux create filters for these types of mirrors.

  Tier 2 Requirements

  - 100Mbit/s bandwidth
  - Above 100GB of storage
  - Sync once an hour on a random minute
  - HTTPS TLS1.2+
  - Sync at the latest every 1h
  - Maintenance can last no longer than 1 month *(after which it gets disabled, not removed)*

  Tier 2 Recommendations

  - Signed ``domain``+``lastupdate``
  - “Service JSON manifest”

**Tier 3** *(EdgeTier)* - A best effort user-facing mirror with the lowest requirements aside from Tier 4.

  Tier 3 Requirements

  - HTTPS TLS1.2+
  - Sync at least once per day
  - Downtime can last no longer than 48 hours *(after which it gets disabled, not removed)*

**Tier 4** *(LocalMirror)* - A fully user-controlled mirror.
    - Not to be listed in official mirror listing

Status Endpoint
^^^^^^^^^^^^^^^

This proposes a new way for mirror operators(s) to communicate technical data to `mirror administrators`_ in an automated fashion. This will allow `mirror administrators`_ to build tooling to automatically detect these technical messages and act on them.

The proposed method is via a JSON `service specification`_. This service specification must be discoverable in a automated fashion via either DNS TXT lookup and/or via a `well known`_ URI. The DNS record used for lookup is ``_archmirror_v1.<mirror domain>`` and the format is a URL to the JSON `service specification`_ file hosted somewhere.

The status endpoint can be hosted either on a secondary domain, a `text storage site`_ such as https://0x0.st or directly on the mirror domain.

The status endpoint can communicate outages, service windows or flag for decommissions.
**Note:** removal or mirror issues must use the `mirror project`_'s issue tracker.

Validation of Ownership
^^^^^^^^^^^^^^^^^^^^^^^

A new way of verifying *(continued)* ownership of a mirror domain is hereby proposed. This is primarily to combat `domain hijacking`_.

The mirror operator(s) must first submit an OpenSSL supported public key under their `GitLab`_ account. After the key is submitted the proposed workflow looks like this:

1. With the private key sign the contents of ``updatetime`` appended with a semicolon ``;`` and the `FQDN`_ of the mirrors domain, example: `1700471141;ftp.lysator.liu.se`
2. Arch Linux then validates the mirror continuously

Mirrors are then automatically hidden if signatures or content is considered invalid. This can also be managed via ``mirrorctl``.

Arch Linux commitment
^^^^^^^^^^^^^^^^^^^^^

The following are proposed commitments from Arch Linux. The commitments aim to ease the way the community interact with Arch Linux in the context of mirrors and mirror management. The task itself is not very complex, but this would also have a positive side effect of ensuring a consistent behaviour across mirrors by providing the following:

- Provide *(rootless)* container based images for different tiers *(with docker-compose)*
- Provide `pacman` package for different tiers *(for EdgeTier this is required)*
- Provide IoT image for hosting mirrors
   - **Note:** *Implement once external dependency of ARM-support is resolved*
- `archinstall`_ profile for mirror hosting
   - Auto-submit GitLab PR/TOML if possible *(after installing)*
- Support more protocols *(not in absurdum)*
   - HTTPS
   - rsync
   - torrent


Tier Structure
^^^^^^^^^^^^^^

This RFC outlines a new proposal for the tier model, along side the naming of the different tiers. The new model consists of promoting more decentralized mirrors both by protocol but also by tier levels.

This aims to combat overcrowding regions with high tier mirrors as well as enhance the user experience. Despite it being uplifting to have many high tier mirrors, it defeats the purpose of the tier model slightly.
The proposal is therefore to reorganize the current T1 in the regions where there's an abundance of them to T2, and this should not be considered a downgrade - but a sideways migration of duties. The same goes for T2 in overpopulated regions where some will be changed to a T3 status.

The migration will depend on gathered service statistics among geographical factors where applicable.

Mirror Protocols
^^^^^^^^^^^^^^^^

This RFC proposes that supported protocols should foremost be defined by the `mirror specification`_. Specifically this RFC aims to define the structure of which torrents could be used between inter-mirror communication.

This aims to be a viable method of offloading mainly the SourceTier and DistributionTier mirrors as the protocol has built-in offloading of data sharing. But torrent-only mirrors will not be allowed in the first version. Instead in such scenarios `seed box`_ endpoints can be used and thus not being listed.

The RFC proposes Arch Linux runs and maintains a torrent tracker for direct announcements. This would allow Arch Linux to prioritize different peer-listing depending on the requesting peer. Tier2 peers could have Tier1 prioritized in the peer listing for instance. It would also allow for geographical optimizations based on known locations of mirrors where applicable.

Future work could be done to allow for `middleware`_ between `pacman`_ and mirrors, allowing for torrent to be a viable source of package downloads in high-capacity connections. A proof of concept for such a scenario was created after `Arch Summit 2023`_ called `pactor`_. The proof of concept was slow and requires further work on `pacman`_ to allow for *"mirrors"* with potential delays.

It could also allow for a `seed box`_ mirror type to be created with the soul purpose of speeding up package distribution between mirrors of all types.

Introducing torrent into the mirror-to-mirror communication, some what nullifies the tier model as all mirrors can simultaneously offload each other. But keeping the tier model could allow Arch Linux to artificially speed up the propagation from the server side.

Mirror Tooling
^^^^^^^^^^^^^^

To facilitate the changes outlined in this RFC, tooling would not only be beneficial but crucial for managing things going forward. To preface, all features workflows of ``mirrorctl`` should also be possible to do manually from the mirror administrators. 

This RFC proposes a tool called ``mirrorctl`` which should have at least the following features:

- Handle signing of proof-of-domain-ownership
- Configuring mirror aspects
- Initializing sync of mirror content
   - Interact with ``repo-add`` *(or using library to create database files)*
- Handle JSON service messages

Create a test-mirror script, proposed to be named ``arch-mirror``.
The purpose of this tooling is to interact with the JSON service messages, validate content and manage status in the mirror listing. If for instance a ``lastseen`` has not been updated for a given time *(based on the "Tier level")*, then mirrors are eligible for being auto-hidden.

Container images for setting up a mirror should be separated into the different responsibilities. And should if possible be rootless, for instance via `podman`_.

Drawbacks
---------

The main drawback would be that old mirrors might not updated to the proposed changes. The RFC has taken this into account and it produces no unwanted results other than old mirrors not supporting new features.

The RFC proposes gradual changes to the mirror layout to allow old mirrors to conform with new requirements. If this change would greatly impact package distribution, a re-evaluation of the process shall be done to minimize the impact of package distribution. 

It would require some more maintenance from Arch Linux staff, specifically the devops team. However, the added workload is considered to be less than or equal to that of the current `mirror administrators`_.

Unresolved Questions
--------------------

- Are the values for the different tiers sane? We propose some not-too-radical requirements on mirrors, but is this in alignment with the community's expectations of mirrors?
- Are there any strong objections against the torrent-protocol being implemented between mirrors? Take into account that we will not allow mirrors with only torrent as a sharing medium as the protocol might be blocked in certain environments.
- Any issues regarding archweb and this migration that we have not thought of?
- Impact on `reflector`_ for instance?
- When T1 have issues and get inactive -> hide downstream mirrors (unless it's a JSON manifest announced service change, then no need until long time has passed)

  - Notify Downstream mirrors about the issue, so they can have a chance to change upstream.
  - Notify X time before upstream gets disabled, so they have time to avoid disabling
  - Can we distribute redistribution of upstream linkages when a upstream goes down?
  - Less spam if the upstream going down -> contacts alternatives and defines it in the service manifest

Suggested future projects
-------------------------

- Create a service that keeps the mirror-listing up to date based on the source of truth *(with parameters)*

Alternatives Considered
-----------------------

Alternative discussions was not mentioned in the `Arch Summit 2023`_ as the conclusion was that this is a good step forward.


.. _scarecely doccumented: https://wiki.archlinux.org/title/DeveloperWiki:NewMirrors
.. _archweb: https://github.com/archlinux/archweb
.. _flyspray: https://bugs.archlinux.org/
.. _mirror administrators: https://gitlab.archlinux.org/groups/archlinux/teams/mirror-administrator/-/group_members
.. _mirror specification: https://gitlab.archlinux.org/archlinux/arch-mirrors/-/tree/main/specs?ref_type=heads
.. _GitLab: https://gitlab.archlinux.org/
.. _mirror project: https://gitlab.archlinux.org/archlinux/arch-mirrors
.. _mirror definition: https://gitlab.archlinux.org/archlinux/arch-mirrors/-/tree/main/specs/mirror_def?ref_type=heads
.. _reflector: https://xyne.dev/projects/reflector/
.. _SLA: https://en.wikipedia.org/wiki/Service-level_agreement
.. _CDN: https://en.wikipedia.org/wiki/Content_delivery_network
.. _service specification: https://gitlab.archlinux.org/archlinux/arch-mirrors/-/tree/main/specs/service_def?ref_type=heads
.. _well known: https://en.wikipedia.org/wiki/Well-known_URI
.. _text storage site: https://en.wikipedia.org/wiki/Pastebin
.. _domain hijacking: https://en.wikipedia.org/wiki/Domain_hijacking
.. _FQDN: https://en.wikipedia.org/wiki/Fully_qualified_domain_name
.. _archinstall: https://github.com/archlinux/archinstall
.. _middleware: https://en.wikipedia.org/wiki/Middleware
.. _pacman: https://wiki.archlinux.org/title/Pacman
.. _Arch Summit 2023: https://md.archlinux.org/
.. _pactor: https://github.com/Torxed/pactor
.. _seed box: https://en.wikipedia.org/wiki/Seedbox
.. _podman: https://wiki.archlinux.org/title/Podman