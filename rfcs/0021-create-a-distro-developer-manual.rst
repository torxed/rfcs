======================================
Create a distribution Developer Manual
======================================

- Date proposed: 2023-08-23
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0021

Summary
-------

This RFC proposes the migration of Arch Linux's operational distro
specifications from existing ArchWiki and other sources to a dedicated GitLab
developer manual repository. The intention is to document how to run the
distribution while leveraging GitLab's collaboration features and streamlined
workflows for maintaining and evolving the resulting specifications. This
change aims to centralize and enhance the efficiency of specification
management while preserving the integrity of the community-contributed content
in the Wiki.

This request for comments is superseding
`RFC-0005 <https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/5>`_ by
extending its scope to handle all distro specifications coherently instead of
handling a single sub section (packaging guidelines) differently to others.

Motivation
----------

The current state of Arch Linux's distro specifications, mostly housed within
the ArchWiki and other dedicated sources (such as repository wikis), has led to
a mixture of operational information and general Linux userspace content. By
collecting the distro specifications in a dedicated Git repository maintaining
the manual, we can centralize the resources in a single place and focus on
the operational aspects of the distribution. This approach aligns with Arch
Linux's transition to utilizing GitLab for various operational processes,
including packaging, bylaws, RFCs and software projects. GitLab's advanced
features such as merge request reviews, threaded discussions, and team
collaboration would improve the maintenance and development of distro
specifications.

Specification
-------------

The proposal involves creating a new Git repository dedicated to the Arch Linux
developer manual. This repository will hold documentation similar in style to the
`Fedora Project's documentation pages <https://docs.fedoraproject.org>`_ and the
`GNOME Developer documentation pages <https://developer.gnome.org/documentation/>`_.

Intended Content
^^^^^^^^^^^^^^^^

The manual consists of a variety of topics, each specifically focused on the
internal operational processes integral to the development of Arch Linux.
This encompasses both, specific guidelines and workflows as well as purely
informational content.

The initial version of the developer manual will focus on various topics, some
of which currently exists in the Wiki, such as:

- `Packaging <https://wiki.archlinux.org/title/DeveloperWiki:How_to_be_a_packager>`_ (including
  `keyring <https://gitlab.archlinux.org/archlinux/archlinux-keyring/-/wikis/home>`_,
  `toolchain maintenance <https://wiki.archlinux.org/title/DeveloperWiki:Toolchain_maintenance>`_,
  `all packaging guidelines <https://wiki.archlinux.org/title/Arch_package_guidelines>`_, etc.)
- `Services offered to staff <https://wiki.archlinux.org/title/DeveloperWiki:Staff_Services>`_
- Release Engineering (including Project releases, ISO, containers, etc.)
- Governance structure and teams

It's important to note that this list is not exhaustive but indicative of the
kind of content the manual will include. The primary objective of this RFC is
to agree on the creation and the overreaching purpose of the manual rather than
detailing an exhaustive list of topics. Further topics that align with this intend
can be added in the future as detailed in the contribution section of this document.

Contribution
^^^^^^^^^^^^

Contributions are categorized in three fundamental groups: trivial, moderate,
and major changes, each defined by their scope and impact.

Trivial Changes
"""""""""""""""

Trivial changes are minor, non-substantial changes such as corrections in style,
typos, and wording. Trivial edits can be applied directly by assigned reviewers
without the need for extensive discussions or announcement on `arch-dev-public`_.
The primary goal is to maintain the developer manual's quality while ensuring
efficiency in the editing process.

To facilitate the initial creation of this manual, the transfer of pre-existing
documentation (at the time of the RFC approval) into the manual shall be
counted as a trivial change.

Moderate Changes
""""""""""""""""

Moderate edits include improvements or changes that preserve the established
context and overall agreed purpose of content in the manual such as packaging
guidelines. An example of this would be adding new flags for reproducible builds
or updating existing recommendations to reflect modern practices, while still
adhering to the currently established status quo.

Parts that have not yet been specified on a distro level, such as packaging
guidelines for a brand new language or tool not yet covered by the core
packaging guidelines, can be submitted as moderate change.

All moderate edits MUST be announced on `arch-dev-public`_ following a process
similar to that used for the `RFC`_, to ensure maximum visibility and community
input. The purpose of the announcement is to call for participation to discuss
the proposed merge-request on GitLab.

Major changes
"""""""""""""
These are substantial changes that involve a significant shift in currently
established guidelines, workflows or the overall intent of a section. Such
changes MUST go through the formal `RFC`_ process. This ensures that major
changes are thoroughly discussed, reviewed and agreed without bypassing the
established `RFC`_ process with the developer manual.

If during the course of discussion a moderate change is determined to have
a major impact or shift, it should then be reclassified as major change and
follow the corresponding process as outlined.

Review
""""""

The initial stage of the review process is overseen by a developer manual
moderator, whose primary responsibility is to ensure that all discussions
adhere to the formalized guidelines and styles of the manual. This role also
accepts trivial changes after careful review and approval. Any member of the
Arch Linux Staff is eligible to volunteer for this moderator role by proposing
themselves on the `arch-dev-public`_ mailing list.

To further enhance the quality and relevance of the manual's content, it
is essential to explicitly involve domain experts in the review process.
Specific topics can have a designated group of experts in their respective
fields, that will be auto assigned to issues and merge requests to oversee
particular topics by default.
For instance, Package Maintainers would be responsible for reviewing and
contributing to the sections on packaging guidelines, while ISO and container
maintainers would focus on the Release Engineering sections. This increases
the visibility of each section and shares the responsibility of the content
accuracy and relevance with those who have the most expertise in those areas.
The granularity of auto assignment can be fine tuned; for instance, specific
language packaging guidelines, such as Rust or Python, can have their own
dedicated subgroups.

Any merge-request rejections MUST be accompanied by a comprehensive comment,
offering a substantial rationale that forms the foundation for initiating
subsequent discussions.

Presentation
============

The documentation pages will be rendered for example using GitLab's pages
pipeline, creating a dedicated subdomain for the manual, taking inspiration
from the successful approach that Fedora has taken. This approach will ensure a
clear separation between general Linux userspace content in the Wiki and
distro-specific operational guidelines, like for example:

- https://docs.fedoraproject.org/en-US/packaging-guidelines/
- https://docs.fedoraproject.org/en-US/engineering/
- https://docs.fedoraproject.org/en-US/program_management/
- https://docs.fedoraproject.org/en-US/council/

The manual MUST be written in a single and consistent flavor to ensure
uniformity and coherence throughout the document.

The manual CAN be authored in Markdown, a recommended format in this proposal
due to its seamless integration with GitLab and its compatibility with various
tools for processing sources files.

The manual SHOULD establish a style guideline in its repository, covering topics
like language register, spelling, and similar. The `Wiki <https://wiki.archlinux.org/title/Help:Style>`_
can provide inspiration of such a style guideline.

Licensing
=========

A suitable license that aligns with our principles of openness MUST be selected
and clearly specified for the manual's content.

The manual SHOULD be distributed under CC BY-SA, unless an alternative
agreement is in place.

If articles are directly taken from ArchWiki, permission must be obtained from
the ArchWiki editorial team to re-license them under a license different from
the GNU Free Documentation License 1.3 or later versions.

Drawbacks
---------

One potential drawback is the migration process itself, which may require
careful planning and execution to ensure the integrity and accuracy of
transferred content. Additionally, some contributors might need time to adapt
to the new platform, but the benefits of GitLab's collaborative features are
expected to outweigh any initial challenges.

Unresolved Questions
--------------------

None.

Alternatives Considered
-----------------------

- **Status Quo:** Maintaining the distro specifications within the existing
  Wiki. However, this approach lacks a streamlined method to designate
  individual teams as code owners for specific sections of the operational
  specifications.

- **Alternative Platforms:** Exploring alternative collaborative platforms
  other than GitLab for hosting the distro specifications. However, the
  advantages of such alternatives would likely be limited compared to GitLab,
  given that GitLab is already the primary collaboration platform for various
  other aspects of Arch Linux's operation.

.. _arch-dev-public: https://lists.archlinux.org/listinfo/arch-dev-public
.. _RFC: https://gitlab.archlinux.org/archlinux/rfcs/
