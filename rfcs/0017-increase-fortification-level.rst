===================================
Increase _FORTIFY_SOURCE level to 3
===================================

- Date proposed: 2023-01-05
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0017

Summary
-------

Adjust packaging CFLAGS from -D_FORTIFY_SOURCE=2 to -D_FORTIFY_SOURCE=3 for
better fortification coverage

Motivation
----------

For a long time, we have been using `-D_FORTIFY_SOURCE=2` in our build flags
to detect C memory management problems at build time.  Recently, glibc (2.34)
added `_FORTIFY_SOURCE=3` to add extra checking. This has been supported in clang
for some time, and is now available in GCC with the release of version 12.

The gains are enhanced buffer size detection and better fortification coverage.

A study [1] showed level 3 improved fortification coverage by nearly 4 times on
average. Extreme examples include the Bash shell going from roughly 3.4% coverage
with level 2 to 47% with level 3. Similarly, sudo went from 1.3% to 49.57%.

[1] https://developers.redhat.com/articles/2022/09/17/gccs-new-fortification-level#2__better_fortification_coverage

Specification
-------------

CFLAGS in the distribution packaging makepkg.conf would change from
`-D_FORTIFY_SOURCE=2` to `-D_FORTIFY_SOURCE=3`.

Some applications use `malloc_usable_size` (despite the glibc manual stating
it is for diagnostic purposes only).  This is currently incompatible with
`_FORTIFY_SOURCE=3`, so these packages will need to continue using level 2 by
modifying `C{,XX}FLAGS` in the PKGBUILD.  A TODO list will be created for all
such packages on adoption of this proposal.

Drawbacks
---------

Some claims of performance overhead have been made, but these are likely
overblown [2]. Most of the checks are at compile time, and this change is being
adopted by OpenSUSE, Fedora and Gentoo (in their hardened profile).

[2] https://siddhesh.in/posts/fortify-source-3-performance.html

Unresolved Questions
--------------------

None.

Alternatives Considered
-----------------------

None.
