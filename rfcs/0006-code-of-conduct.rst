---
date: 2021-10-23
weight: 6
---

===============================================
Adoption of a distribution-wide Code of Conduct
===============================================

- Date proposed: 2021-09-15
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/6

Summary
-------

The adoption of a distribution-wide Code of Conduct (CoC) helps to describe the
social contract by which communication takes place on the various communication
channels offered by Arch Linux. This document describes the current CoC, its
purpose and location and how to interact with it.

Motivation
----------

A Code of Conduct is a useful document to describe the *"norms, rules, and
responsibilities or proper practices of an individual party or an
organization"* (see `Wikipedia article
<https://en.wikipedia.org/wiki/Code_of_conduct>`_). Arch Linux is a community
of entities, that consists of direct participants such as `developers
<https://archlinux.org/people/developers/>`_, `trusted users
<https://archlinux.org/people/trusted-users/>`_, `support
staff <https://archlinux.org/people/support-staff/>`_ and users that
communicate with one another over various channels. As with all social
constructs, the Arch Linux community is not immune to disagreements on
technical or personal level.

To maintain a respectful tone and cooperation a Code of Conduct is used in
various organizations (e.g. see `Fedora Code of Conduct
<https://docs.fedoraproject.org/en-US/project/code-of-conduct/>`_ or `Debian
Code of Conduct <https://www.debian.org/code_of_conduct>`_) to outline accepted
behavior within a community.

Historically, a Code of Conduct document for Arch Linux has been written,
extended and maintained by several staff members `in the Arch Wiki
<https://wiki.archlinux.org/index.php?oldid=648541&date-range-to=2021-01-09&action=history>`_.
This document, although widely used throughout the distribution on e.g. the
forums, the mailing lists, the bug tracker, official IRC channels and the wiki
has not been officially ratified.

`The current version of this document
<https://gitlab.archlinux.org/archlinux/service-agreements/-/blob/fc8b06200257ee55e060277c94d756ddfe77632c/content/docs/code-of-conduct.md>`_
is now part of `Arch Linux's Service Agreements
<https://gitlab.archlinux.org/archlinux/service-agreements>`_.

Specification
-------------

The `Arch Linux Code of Conduct
<https://terms.archlinux.org/docs/code-of-conduct/>`_ is part of and defined in
the distribution's `Service Agreements <https://terms.archlinux.org/>`_.
It describes the norms and rules by which to conduct oneself when participating
in the various communication channels that Arch Linux offers. Additionally it
provides information on the enforcement of the rules, as well as whom to
contact in case of misconduct.

As a central document to the distribution it is hereby officially adopted in
`its current form
<https://gitlab.archlinux.org/archlinux/service-agreements/-/blob/fc8b06200257ee55e060277c94d756ddfe77632c/content/docs/code-of-conduct.md>`_,
so that it is recognized and valued by all direct participants and users.

The Code of Conduct is a living document that may change over time. Changes are
applied by merge request towards the `Service Agreements repository
<https://gitlab.archlinux.org/archlinux/service-agreements/>`_. Any
contributions follow the repository's contribution guidelines.

The document is part of the Arch Linux Service Agreements and as such changes
to it do not go into effect right away, but require the distribution to
notify all service users in advance in accordance with the `Terms of Service
Amendments section
<https://terms.archlinux.org/docs/terms-of-service/#_amendments>`_.

Drawbacks
---------

Code of Conduct documents may be abused by individuals or organizations to
achieve the means of becoming immune to criticism (e.g. see `Straw man
<https://en.wikipedia.org/wiki/Straw_man>`_ or `Tu quoque
<https://en.wikipedia.org/wiki/Tu_quoque>`_ fallacies).

Unresolved Questions
--------------------

Alternatives Considered
-----------------------

Over the years conflicts among members of the Arch Linux community have been
raised, evaluated and (hopefully) solved over the various communication
channels.
However, a shared document, such as a Code of Conduct is without alternative
when attempting to apply the same rules to all members of a community, as
otherwise they would vary depending on communication channel or group within
the community.
