===============
Arch Linux RFCs
===============

A Request for Comment (`RFC
<https://en.wikipedia.org/wiki/Request_for_Comments>`_) is a way for Arch Linux
contributors to propose, design and discuss new features and changes in
project direction in a focused environment.

RFCs start as merge requests in:
https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests

Once accepted, they can be found in:
https://gitlab.archlinux.org/archlinux/rfcs/-/tree/master/rfcs

What does an RFC do?
--------------------

The RFC process is intended to bring focus and structure to discussions of
important changes and new features in Arch Linux. It provides a space for
sharing and collaborating on ideas and design documents, keeping community
discussion organized and focused on the goal of reaching a decision.

Visibility and inclusivity are important values for RFCs. They are advertised
to both staff and user communication channels so everyone can participate.

An RFC is only a design document, and not a single line of code needs to be
written for the discussion to be approved, although prototyping can be a good
idea. An approved RFC has no timeline for when it needs to be implemented.

Who can file an RFC?
--------------------

Anyone can file an RFC. However, the following restriction exists for
contributors, that are not Arch Linux Staff:

At least one Arch Linux Staff member has to support the RFC and must be
willing to work on it in a joint effort with the contributor.

When should I file an RFC?
--------------------------

If you have an idea for something and are unsure if it needs an RFC, the best
way to find out is to just ask (e.g. by `opening an issue in this repository
<https://gitlab.archlinux.org/archlinux/rfcs/-/issues/new>`_ and filling out
the "idea" template).
An RFC is much more likely to succeed if it begins life as a discussion, and
collaborators are encouraged to start off by talking to other project members
before submitting an RFC. This can also help gauge early support for the
feature and work out any obvious issues or design decisions.

RFCs are used for proposing "substantial" changes. The meaning of "substantial"
is subjective, but a good start would be anything that benefits from design
discussion. Put a different way, it is recommended to file an RFC if the design
or implementation is not immediately clear, or if there are drawbacks or
potential consequences that require discussion first. These might be worth
consideration for an RFC:

- New features
- Breaking changes
- Organizational changes, including changes to the RFC process itself

Proposing an RFC
----------------

Step 0: Decide if you should file an RFC
________________________________________

As an RFC author, it is best if you only have one RFC of yours open at any
moment. This restriction is a guideline, but you are advised to be respectful
of other contributors who would also like their RFCs to receive attention, and
to be cognizant of the potential cost of spreading the community's attention
thinly across a multitude of proposals.

  .. note::

    To view previous or current ideas, have a look at the `issues
    <https://gitlab.archlinux.org/archlinux/rfcs/-/issues>`_.

Step 1: Fork this repository
____________________________

Fork this repository if you have not already.

Step 2: Write RFC
_________________

Copy **rfcs/0000-template.md** to **rfcs/0000-<my-proposal-title>.md** and begin
working on the RFC.

Please put a lot of thought into your writing. A great RFC explains and
justifies its motivations and carefully considers all drawbacks and
alternatives. Show the reader what the problems are and how the proposal
concretely and practically addresses them.

Step 3: File merge request
__________________________

Create a merge request toward https://gitlab.archlinux.org/archlinux/rfcs. The
open merge request itself **is** the RFC. Discussion of the RFC is held in the
comments of the merge request.

  .. note::

    When creating the merge request, make sure to tick the **Allow commits from
    members who can merge to the target branch.** checkbox in the
    **Contribution** section.

After the merge request is created, the merge request number (e.g. 42) is your RFC
number. Then do the following:

1. In the branch, rename the file from **rfcs/0000-<my-proposal-title>.md** to
   **rfcs/0042-<my-proposal-title>.md**. This makes all the proposals show up
   in ascending chronological order when someone visits the `rfcs <rfcs/>`_
   directory.
2. In the file itself, there is a bullet point labeled **RFC MR:**. Update this
   so that it links to the merge request. This ensures that the discussion is
   immediately accessible from the RFC text.

These are minor housekeeping steps that keep the RFC a nice, tidy, and easily
navigable archive. If you make a mistake in this process, it is no huge issue -
someone else can fix it for you.

Step 4: Announce
________________

After the RFC is ready to go, please announce it to `arch-dev-public
<https://lists.archlinux.org/hyperkitty/list/arch-dev-public@lists.archlinux.org/>`_.

  .. note::

    If you lack the permission to send mail to the mailing list yourself, the
    person from Arch Linux Staff, that is supporting your RFC, will do it for
    you.

Here is a template:

.. code:: email

  Subject: RFC: Deprecate BadFeature

  A new RFC (request for comment) has been opened here:

  https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/42

  Please visit the above link for discussion.

  Summary: <insert the summary section from your RFC here>

Discussing an RFC
-----------------

Discussion about the RFC should happen on the RFC merge request on Arch Linux's
GitLab. If discussions happen outside the GitLab thread, it is best to link and
summarize them in the RFC thread to make the conversations easier to track.

The RFC will likely undergo numerous revisions in the discussion process.

While discussing an RFC, it is everyone's responsibility to work towards
**reaching a conclusion**. When you make a post to the RFC thread, focus on the
contents and implications of the RFC only.

Here are some guidelines:

- **Think about these important questions:**
    - Do you agree with the proposal in its current form?
    - Are concerns and questions adequately addressed?
    - Do you have suggestions for how the RFC can be revised or expanded,
      especially in the Unresolved Questions section?
- **Do not be shy:** Even if you are not an active Arch Linux contributor, or
  if you do not feel "qualified" to discuss the topic, please feel free to
  directly state your opinion on an RFC, especially if it affects you in some
  way.
- **Stay on topic:** Starting or continuing tangential or off-topic discussions
  is disrespectful to the RFC author and is strongly discouraged. Such
  discussions often dilute the thread and make it hard to identify what the
  community's decision should be. The purpose of the RFC thread is to reach a
  conclusion, and any commentary that does not contribute to that is damaging
  to the RFC process.
- **Civility and respect:** Like all other discussions that happen in the Arch
  Linux community, RFC discussions are governed by our `Code of Conduct
  <https://terms.archlinux.org/docs/code-of-conduct/>`_.
  Creating an RFC is a laborious project. Commenting on an RFC should be done
  with respect and empathy for the hard work that the author has put into it.
- **Avoid scope creep:** You should make suggestions on how the RFC can be
  revised or expanded, but be careful not to expand it too much and develop it
  into something far more ambitious than was originally intended.

Finalizing an RFC
-----------------

When the RFC seems ready to conclude, all staff members may make a motion for a
Final Comment Period. The Final Comment Period lasts 14 days, and has a
*disposition* to either **pass** or **reject**.

To make a motion for a Final Comment Period, leave a comment on the merge
request thread or use GitLab's approval functionality (for a positive
affirmation). The Final Comment Period should also be announced on
`arch-dev-public
<https://lists.archlinux.org/hyperkitty/list/arch-dev-public@lists.archlinux.org/>`_.

Here is a template:

.. code:: email

  Subject: RFC Final Comment Period: Deprecate BadFeature

  An RFC has now entered Final Comment Period. In 14 days, discussion will end
  and the proposal will either be accepted, rejected or withdrawn:

  https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/42

  Please visit the above link for discussion.

  Summary: <insert the summary section from your RFC here>

When should the Final Comment Period start? There does not need to be a
consensus with everyone involved (which is usually impossible), but the RFC
should be in a state where the author feels that the writing effort is
complete, the arguments have been clearly articulated, discussions are resolved
and drawbacks acknowledged in the RFC's text, and there is not a strong
consensus *against* the proposal.

Sometimes, RFCs can reach points of disagreement where no clear consensus is in
sight. Unfortunately, there is no way that this RFC process can solve such
issues, and these have to be handled on a case-by-case basis.

If no important developments occur during this time frame, someone with write
access merges (for passed RFCs) or closes (for rejected RFCs) the RFC PR.

At any point, the RFC author can also choose to **withdraw** an RFC, with no
14-day grace period necessary.

Implementation of an RFC can start before, during, or after it has been
approved. In fact, if you propose an RFC, you do not need to be the person to
implement it, but usually the implementer is the same person as the RFC author.

About the RFC process
---------------------

This is not a legal document, and you should not spend a lot of time analyzing
its wording. This process is intentionally quite informal and subjective.
Situations will come up that may not have been anticipated by this article, and
they should be handled on a case-by-case basis.

If something is ambiguous in this article, that means it is left up to
discussion. If something is a persistent cause of confusion, then it may be
wise to amend the RFC process itself for clarification.

License
-------

All contributions are licensed by their respective authors under the terms of
the `CC-BY-SA 4.0 License
<https://creativecommons.org/licenses/by-sa/4.0/legalcode>`_.
